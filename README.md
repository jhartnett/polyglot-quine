# Polyglot Quine
This project was completed as an exercise in programming esoterica.
It is a combination of the following two ideas:
1. In computing, a **polyglot** is a computer program or script written in a valid 
form of multiple programming languages, which performs the same operations or 
output independent of the programming language used to compile or interpret it 
(source [Wikipedia](https://en.wikipedia.org/wiki/Polyglot_(computing))).
2. A **quine** is a non-empty computer program which takes no input and produces a copy
of its own source code as its only output (source [Wikipedia](https://en.wikipedia.org/wiki/Quine_(computing))).

A polyglot quine is thus a program which runs unmodified in multiple programming
languages, printing its source code in each.

In this case, the polyglot quine runs in `DOS`, `Python 3`, and `BASH`. The code
is contained in the `poly_quine.*` files, which are identical. The source has been
modified to minimize the number of characters. Multiple files are
provided for syntax highlighing purposes in IDEs.
The run commands are the following:
* Bash - `bash poly_quine.sh`
* Python - `python poly_quine.py`
* DOS - `cmd /Q /C poly_quine.cmd`

Note that the extra flags required by DOS are used to run a file (instead of opening a prompt) 
and to suppress the default command echoing.

The `watcher.js` script is provided for development purposes. It monitors changes
to any of the files, updating the others and rerunning them to ensure proper output.
Its dependencies are installed using `npm install` and it is run using `npm start`.