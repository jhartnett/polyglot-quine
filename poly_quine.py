# 2>nul || set ^
body='# 2^>nul ^|^| set ^^\nbody="{0}"\n""":" # 2^>nul\n# 2^>nul ^|^| goto dos\nouter=${body//$"\\x22"/$"\\x27"}\nouter=${outer//^^^^/^^}\nouter=${outer//^^^>/^>}\nouter=${outer//^^^|/^|}\nouter=${outer//\\%%%%/%%}\nouter=${outer//$"\\x40"/@}\nouter=${outer//\\\\\\\\/\\\\}\nouter=${outer//$"\\x5cn"/$"\\x0d"}\nbody=${outer//{0\\}/$body}\necho ${body}\nexit\n:dos\nsetlocal EnableDelayedExpansion\ncmd /c exit 34\nset double=%%=ExitCodeAscii%%\ncmd /c exit 39\nset single=%%=ExitCodeAscii%%\ncmd /c exit 64\nset at=%%=ExitCodeAscii%%\nset body=@body:~1,-1@\nset outer=@body:~0,-2@\nset outer=@outer:%%double%%=%%single%%@\nset outer=@outer:^^^^=^^^^^^^^@\nset outer=@outer:^^^>=^^^^^^^>@\nset outer=@outer:^^^|=^^^^^^^|@\ncall set outer=%%%%outer:%%at%%=^^^^@%%%%\nset outer=@outer:\\^^n=^^\n\n@\nset outer=@outer:\\\\=\\@\nset inner=@body:^^^^=^^^^^^^^^^^^^^^^@\nset inner=@inner:%%%%=%%%%%%%%@\nset inner=@inner:^^^>=^^^^^^^^^^^^^^^>@\nset inner=@inner:^^^|=^^^^^^^^^^^^^^^|@\nset body=@outer:{^^0}=%%inner%%@\necho @body@\nexit\n"""\ninner = body.replace("\\\\", "\\\\\\\\")\ninner = inner.replace(chr(10), "\\\\"+"n")\nouter = body.replace(chr(34), chr(39))\nouter = outer.replace("^^"*2, "^^")\nouter = outer.replace("^^^>", "^>")\nouter = outer.replace("^^^|", "^|")\nouter = outer.replace("%%"*2, "%%")\nouter = outer.replace(chr(64), "@")\nbody = outer.replace("{"+str(0)+"}", inner)\nprint(body, end="")\n'
''':' # 2>nul
# 2>nul || goto dos
outer=${body//$'\x22'/$'\x27'}
outer=${outer//^^/^}
outer=${outer//^>/>}
outer=${outer//^|/|}
outer=${outer//\%%/%}
outer=${outer//$'\x40'/!}
outer=${outer//\\\\/\\}
outer=${outer//$'\x5cn'/$'\x0d'}
body=${outer//{0\}/$body}
echo ${body}
exit
:dos
setlocal EnableDelayedExpansion
cmd /c exit 34
set double=%=ExitCodeAscii%
cmd /c exit 39
set single=%=ExitCodeAscii%
cmd /c exit 64
set at=%=ExitCodeAscii%
set body=!body:~1,-1!
set outer=!body:~0,-2!
set outer=!outer:%double%=%single%!
set outer=!outer:^^=^^^^!
set outer=!outer:^>=^^^>!
set outer=!outer:^|=^^^|!
call set outer=%%outer:%at%=^^!%%
set outer=!outer:\^n=^

!
set outer=!outer:\\=\!
set inner=!body:^^=^^^^^^^^!
set inner=!inner:%%=%%%%!
set inner=!inner:^>=^^^^^^^>!
set inner=!inner:^|=^^^^^^^|!
set body=!outer:{^0}=%inner%!
echo !body!
exit
'''
inner = body.replace('\\', '\\\\')
inner = inner.replace(chr(10), '\\'+'n')
outer = body.replace(chr(34), chr(39))
outer = outer.replace('^'*2, '^')
outer = outer.replace('^>', '>')
outer = outer.replace('^|', '|')
outer = outer.replace('%'*2, '%')
outer = outer.replace(chr(64), '!')
body = outer.replace('{'+str(0)+'}', inner)
print(body, end='')
