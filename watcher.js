const Chokidar = require('chokidar');
const Chalk = require('chalk');
const Fs = require('fs');
const ChildProcess = require('child_process');
const pack = require('./package.json');

const readFile = promisify(Fs.readFile);
const writeFile = promisify(Fs.writeFile);

let config = pack.polyglot.languages;
{
    let names = '';
    for(let lang of config)
        names += lang.name[0];
    console.log(names);
}

let ignore = false;

Chokidar.watch(config.map(lang => lang.file), {
    persistent: true,
    cmd: '.'
}).on('change', path => {
    if(ignore)
        return;
    onChange(path).catch(console.error);
});

async function onChange(path) {
    let body = await readFile(path, {encoding: 'utf8'});
    try{
        ignore = true;
        await Promise.all(
            config
                .map(lang => lang.file)
                .filter(file => file !== path)
                .map(async file => {
                    await writeFile(file, body);
                })
        );
    }finally{
        ignore = false;
    }
    let results = await Promise.all(
        config
            .map(async lang => {
                return await check(lang.cmd, lang.file, body);
            })
    );
    let str = '';
    for(let result of results)
        str += (result ? Chalk.green : Chalk.red)('█');
    console.log(str);
}

function check(cmd, file, body) {
    return new Promise((fulfill, reject) => {
        ChildProcess.exec(`${cmd} ${file}`, (err, stdout, stderr) => {
            if(err)
                reject(new Error(err));
            else if(stderr.length !== 0)
                reject(new Error(stderr));
            else
                fulfill(body === stdout);
        });
    });
}

function promisify(func) {
    return function(...args) {
        return new Promise((fulfill, reject) => {
            func(...args, (err, ...args) => {
                if(err)
                    reject(new Error(err));
                else
                    fulfill(...args);
            });
        });
    };
}